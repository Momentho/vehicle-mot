# SOFIDIS - VEHICLE MOT
Little project realised for the Huboo tech test.

#### For the test I decided to:

- **Use Vue.js** given that it's the technology used by the team - fist time for me, so a lot to have fun with :muscle:
- Keep the API call inside the main component used for the page .. something to avoid, but quicker to manage :poop:
- Show a little bit of **atomic design** architecture and **BEM** 	:eye: 
- Mixed rem and px copnverted in rem with a very handy function :boom:

#### Important note:
Due to the time, some architecture and accessibility best practices have not been used, **happy to discuss where and why**.

For the same reason and the fact that Vue.js is a bit different from Angualr, some of Vue best practices may not have been used.

## Project setup
Node version: 12.13.0
The project was scaffolded with the vue-cli.

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

